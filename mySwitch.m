- (void) switchIsChanged:(UISwitch *)paramSender{
   
  NSLog(@"Sender is = %@", paramSender);
   
  if ([paramSender isOn]){
      NSLog(@"The switch is turned on.");
  } else {
      NSLog(@"The switch is turned off.");
  }
   
}
 
- (void)viewDidLoad{
  [super viewDidLoad];
   
  /* Make sure our view is white */
  self.view.backgroundColor = [UIColor whiteColor];
   
  /* Create the switch */
  self.mySwitch = [[UISwitch alloc] initWithFrame:
                   CGRectMake(100, 100, 0, 0)];  
  [self.mySwitch setOn:YES];
  [self.view addSubview:self.mySwitch];
   
  [self.mySwitch addTarget:self
                    action:@selector(switchIsChanged:)
          forControlEvents:UIControlEventValueChanged];
   
}